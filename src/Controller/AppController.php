<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AppController extends AbstractController
{
    public function content(Request $request)
    {
        $routeConfig = $request->attributes;
        $params = [];

        // make all non-private (i.e. not underscore-prefixed) route params available to template
        foreach ($routeConfig->all() as $configName => $configValue) {
            if ($configName[0] !== '_') {
                $params[$configName] = $configValue;
            }
        }

        // create full view title from route title and app title (unless `fullTitle` is set in route config)
        $fullTitle = $routeConfig->get('fullTitle');
        $routeTitle = $routeConfig->get('title');
        $appTitle = $this->getParameter('app.title');
        $titleGlue = $this->getParameter('app.titleGlue') ?? ' - ';
        $params['viewTitle'] = $fullTitle ?? rtrim($routeTitle . $titleGlue . $appTitle, ' -/:');

        // render the view
        $response = $this->render($routeConfig->get('template'), $params);
        $response->headers->set('X-App-Path', $request->getPathInfo());
        return $response;
    }
}
