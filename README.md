# Vuefony

Boilerplate project setup based on Symfony 4, Vue 2, Vuetify 2.

## Installation

* Run `composer create-project bnowack/vuefony PROJECT-DIR --repository='{"type": "gitlab","url": "https://gitlab.com/bnowack/vuefony.git"}'`.
    * Replace `PROJECT-DIR` with your target directory name.
* Enter your project directory.
* Run `npm install` or `yarn install`.

## Development

* Adjust `app.*` values in `config/services.yaml`.
* Reset or adjust `README.md`, `CHANGELOG.md`, `LICENSE`, `composer.json`.
* See Symfony, Vue, Vuetify websites for respective documentation.

### Release creation

* Create a `.env.prod.php` file from `.env.prod.php-template`.
* See `Script shortcuts` section below for build and test server scripts.

### Script shortcuts

* The `scripts` section in `package.json` contains useful development script shortcuts:
   * `npm run dev-server` starts a PHP development server at `localhost:8080`.
   * `npm run ui-dev` starts an [HMR-enabled](https://webpack.js.org/concepts/hot-module-replacement/) webpack dev-server at `localhost:8081`.
     * The php development server at `localhost:8080` will auto-discover the webpack server.
     * Add `--host IP-ADDRESS` to the script if you want to bind HMR to a particular IP address (e.g. for mobile development, when the app is not accessed via localhost).
   * `npm run ui-prod` builds production-ready frontend bundles.
   * `npm run build` creates a versioned release in `PROJECT-DIR/../releases/VERSION` (using `app.version` from `config/services.yaml`)
     and a copy of the release in `PROJECT-DIR/../releases/latest`.
   * `npm run test-server-build` builds a Docker container for testing the latest release.
   * `npm run test-server-up` starts a Docker instance at `localhost:8082` serving `PROJECT-DIR/../releases/latest`.
   * `npm run test-server-down` stops a running Docker test server.
   * `npm run test-server-cli` starts a CLI to the Docker test server.
