import "../css/app.scss";
import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import VueResource from "vue-resource";
import _debounce from "lodash.debounce";
import _throttle from "lodash.throttle";
import Vuetify, { VLayout, VFlex } from "vuetify/lib";
import "@mdi/font/css/materialdesignicons.css";
import "typeface-open-sans";
import "typeface-roboto";

Vue.use(Vuex);
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuetify, {
    components: {
        VLayout,
        VFlex
    }
});

// register top-level components
Vue.component("app-layout", () =>
    import(/* webpackChunkName: "app-components" */ "../vue/app-layout")
);

// start Vue
setTimeout(() => {
    app.throttle = _throttle;
    app.debounce = _debounce;
    app.vue = new Vue({
        el: "#app-container",
        // vuex
        store: new Vuex.Store({}),
        // vue-resource
        http: {
            root: app.base,
            headers: {
                "X-Requested-With": "XMLHttpRequest"
            },
            credentials: true
        },
        // vue-router
        router: new VueRouter({
            mode: "history",
            base: app.base
        }),
        // vuetify
        vuetify: new Vuetify({
            icons: {
                iconfont: "mdi"
            },
            theme: {
                themes: {
                    light: {
                        primary: "#1976D2",
                        secondary: "#424242",
                        accent: "#82B1FF",
                        error: "#FF5252",
                        info: "#2196F3",
                        success: "#4CAF50",
                        warning: "#FFC107"
                    }
                }
            }
        })
    });
}, 100);
