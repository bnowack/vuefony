# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
* \-

## [1.0.0] - 2019-06-01

### Added

* `composer require symfony/web-server-bundle --dev`
* `composer require encore`
* `yarn add @symfony/webpack-encore --dev`
* `yarn add webpack-notifier --dev`
* `yarn add sass --dev`
* `yarn add sass-loader --dev`
* `yarn add vue vue-loader vue-template-compiler --dev`
* `yarn add native-promise-only --dev`
* `composer require symfony/twig-bundle`
* `yarn add lodash.debounce lodash.throttle --dev`
* `yarn add vuex --dev`
* `yarn add vue-router --dev`
* `yarn add vue-resource --dev`
* `yarn add vuetify@^2.0.0 core-js deepmerge fibers vuetify-loader --dev`
* `yarn add @mdi/font --dev`
* `yarn add typeface-open-sans typeface-roboto --dev`
* development and build scripts (see `README.md`)
* app manifest (`/manifest.json`) route and response
* basic home and imprint views
