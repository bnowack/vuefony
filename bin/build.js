let readYaml = require("read-yaml");
let which = require("which");
let builder = require("@bnowack/deployer").builder;

// initialize
builder.init({
    path: "../releases",
    version: readYaml.sync("config/services.yaml", {}).parameters[
        "app.version"
    ],
    env: {
        APP_ENV: "prod",
        COMPOSER_HOME: which.sync("composer")
    }
});

builder
    // reset directory
    .resetDirectory(builder.config.buildDir)

    // build prod env file
    .copy(".env.prod.php", `${builder.config.buildDir}/.env.local.php`)

    // copy app files (before `composer install` is run)
    .copy("config", `${builder.config.buildDir}/config`)
    .copy("src", `${builder.config.buildDir}/src`)
    .copy("templates", `${builder.config.buildDir}/templates`)
    //.copy('translations', `${builder.config.buildDir}/translations`)
    .copy("public/.htaccess", `${builder.config.buildDir}/public/.htaccess`)
    .copy("public/index.php", `${builder.config.buildDir}/public/index.php`)
    // init cache directory
    .createDirectory(`${builder.config.buildDir}/var/cache/prod`)
    .chmod(`${builder.config.buildDir}/var`, 0o777)

    // install dependencies
    .copy("composer.json", `${builder.config.buildDir}/composer.json`)
    .composer(
        "install --no-dev --optimize-autoloader --classmap-authoritative --no-interaction"
    )

    // reset auto-generated framework files
    .emptyDirectory(`${builder.config.buildDir}/var/cache/prod`)
    .remove(`${builder.config.buildDir}/assets`)
    .remove(`${builder.config.buildDir}/bin`)
    .remove(`${builder.config.buildDir}/config/packages/+(dev|test)`)
    .remove(`${builder.config.buildDir}/.env`)
    .remove(`${builder.config.buildDir}/.gitignore`)
    .remove(`${builder.config.buildDir}/composer.lock`)
    .remove(`${builder.config.buildDir}/symfony.lock`)

    // remove superfluous vendor directories and files
    .remove(
        `${
            builder.config.buildDir
        }/vendor/**/+(doc|docs|tests|Tests|examples|Examples)`
    )
    .remove(
        `${
            builder.config.buildDir
        }/vendor/**/+(README.md|README|LICENSE|CHANGELOG.md|.gitignore|.editorconfig)`
    )

    // build frontend files
    .copy("package.json", `${builder.config.buildDir}/package.json`)
    .copy("yarn.lock", `${builder.config.buildDir}/yarn.lock`)
    .copy("assets", `${builder.config.buildDir}/assets`)
    .copy("webpack.config.js", `${builder.config.buildDir}/webpack.config.js`)
    .yarn("install") // including dev dependencies
    .yarn("encore prod")
    .remove(`${builder.config.buildDir}/node_modules`)
    .remove(`${builder.config.buildDir}/package.json`)
    .remove(`${builder.config.buildDir}/yarn.lock`)
    .remove(`${builder.config.buildDir}/assets`)
    .remove(`${builder.config.buildDir}/webpack.config.js`)

    // build test version
    .resetDirectory(builder.config.path + "/latest")
    .copy(builder.config.buildDir, builder.config.path + "/latest");
